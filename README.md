# Economique API

## How to run:

1. Clone the repo `git clone https://gitlab.com/maximovic/economique_api.git`

2. Navigate to the project directory `cd ./economique_api`

3. Run `npm install` to install all the dependencies.

4. You need to have postgres server running on your machine.

5. Run the sql query from the **init.sql** file to create the *economique* database.

6. Edit the **./config/dev.yaml** and **./config/migrate.yaml** file according to your database settings (username, password, etc).

7. Run `npm run setup` this will run the migrations and start the app