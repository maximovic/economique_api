exports.up = db => {
	return db.query(
		db.sql`
		CREATE TABLE IF NOT EXISTS users(
			id SERIAL PRIMARY KEY UNIQUE NOT NULL, 
			username VARCHAR NOT NULL,
			email VARCHAR UNIQUE NOT NULL,
			password VARCHAR,
			verification_code VARCHAR,
			verified_at TIMESTAMP,
			created_at TIMESTAMP NOT NULL
		)`
	);
};

exports.down = db => {
	return db.query(db.sql`DROP TABLE IF EXISTS users CASCADE`);
};
