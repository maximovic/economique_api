const colors = require('colors');

colors.setTheme({
	info: 'green',
	verbose: 'cyan',
	debug: 'gray',
	warn: 'yellow',
	error: 'red',
});

function Log(service) {
	this.info = (...args) => {
		console.log(`${new Date().toISOString()} ` + `info: [${service.name}] ${args}`.info);
	};

	this.verbose = (...args) => {
		console.log(`${new Date().toISOString()} ` + `verbose: [${service.name}] ${args}`.verbose);
	};

	this.debug = (...args) => {
		console.log(`${new Date().toISOString()} ` + `debug: [${service.name}] ${args}`.debug);
	};

	this.warn = (...args) => {
		console.log(`${new Date().toISOString()} ` + `warn: [${service.name}] ${args}`.warn);
	};

	this.error = (...args) => {
		console.log(`${new Date().toISOString()} ` + `error: [${service.name}] ${args}`.error);
	};
}

module.exports = {
	Log,
};
