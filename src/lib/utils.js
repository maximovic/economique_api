exports.getDependencies = (dependencies, required) => {
	return Object.keys(dependencies)
		.filter(key => required.includes(key))
		.reduce((obj, key) => {
			obj[key] = dependencies[key];
			return obj;
		}, {});
};

exports.promiseGuard = fn => {
	return new Promise((resolve, reject) => {
		try {
			const value = fn();
			return resolve(value);
		} catch (err) {
			return reject(err);
		}
	});
};

exports.randomString = (length, pool) => {
	return Array(length)
		.join()
		.split(',')
		.map(() => {
			return pool.charAt(Math.floor(Math.random() * pool.length));
		})
		.join('');
};
