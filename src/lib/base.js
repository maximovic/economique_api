'use-strict';

const { EventEmitter } = require('events');

const { Log } = require('./log');

/****************************************************************
 *	Base function to be inherited by all app services.
 *	Provides all basic functionalities (log, start, stop, emit).
 */
function Service() {}

Service.prototype.emitter = new EventEmitter();

Service.prototype.start = () => {};
Service.prototype.stop = () => {};
Service.prototype.emit = (event, data) => Service.prototype.emitter.emit(event, data);
Service.prototype._now = () => new Date();

Service.superOf = fn => {
	fn.prototype = Object.create(Service.prototype);

	// attach logger
	fn.prototype.log = new Log(fn);

	Object.defineProperty(fn.prototype, 'constructor', {
		value: fn,
		enumerable: false,
		writable: true,
	});
};

module.exports = {
	Service,
};
