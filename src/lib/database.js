const { createPool, sql } = require('slonik');

const { Service } = require('./base');

class DatabaseConfig {
	constructor(/** DatabaseConfig */ source) {
		this.user = source.user;
		this.password = source.password;
		this.host = source.host;
		this.port = source.port;
		this.database = source.database;
		this.url = `postgres://${source.user}:${source.password}@${source.host}:${source.port}/${source.database}?sslmode=require`;
		// this.url = process.env.DATABASE_URL + '?sslmode=require';
	}
}

function Database(config) {
	config = new DatabaseConfig(config);

	this.start = () => {
		return Promise.resolve()
			.then(() => {
				this.sql = sql;
				this.pool = createPool(config.url);

				return this.pool.connect(conn => {
					return conn.query(sql`SELECT LIMIT 1`).then(() => {
						this.log.info('connected to database');
					});
				});
			})
			.catch(err => {
				this.log.error(err);
			});
	};

	this.query = query => {
		this.log.debug(`${query.sql}, [${query.values}]`);
		return this.pool.query(query);
	};

	this.exists = query => {
		this.log.debug(`${query.sql}, [${query.values}]`);
		return this.pool.exists(query);
	};

	this.one = query => {
		this.log.debug(`${query.sql}, [${query.values}]`);
		return this.pool.one(query);
	};

	this.maybeOne = query => {
		this.log.debug(`${query.sql}, [${query.values}]`);
		return this.pool.maybeOne(query);
	};

	this.many = query => {
		this.log.debug(`${query.sql}, [${query.values}]`);
		return this.pool.many(query);
	};

	this.any = query => {
		this.log.debug(`${query.sql}, [${query.values}]`);
		return this.pool.any(query);
	};
}

Service.superOf(Database);

module.exports = {
	Database,
};
