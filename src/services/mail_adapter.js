const { getDependencies } = require('../lib/utils');

const { Service } = require('../lib/base');
const { EVENTS } = require('../types/consts');
const { VerificationMail } = require('../types/mails');

class MailAdapterConfig {
	constructor(/** MailAdapterConfig */ source) {
		this.clientURL = source.clientURL;
	}
}

function MailAdapter(config, deps) {
	config = new MailAdapterConfig(config);
	deps = getDependencies(deps, ['authService', 'mailerService']);

	this.start = () => {
		deps.authService.emitter.on(EVENTS.user_verfication, data => {
			const mail = new VerificationMail({
				username: data.user.username,
				code: data.user.verification_code,
			});

			return deps.mailerService.sendEmail(data.user.email, mail);
		});
	};
}

Service.superOf(MailAdapter);

module.exports = {
	MailAdapter,
};
