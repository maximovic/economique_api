/* eslint-disable no-undef */

const lodash = require('lodash');
const puppeteer = require('puppeteer');

const { Service } = require('../lib/base');

class ScraperServiceConfig {
	constructor(/** ScraperServiceConfig */ source) {
		this.baseURL = source.baseURL;
	}
}

function ScraperService(config, deps) {
	config = new ScraperServiceConfig(config);

	let _browser, _page;

	this.start = () => {
		return puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox']}).then(browser => {
			_browser = browser;
			return browser.newPage().then(page => {
				return page.setRequestInterception(true).then(() => {
					page.on('request', req => {
						if (
							req.resourceType() === 'script' ||
							req.resourceType() === 'image' ||
							req.resourceType() === 'stylesheet' ||
							req.resourceType() === 'font'
						) {
							return req.abort();
						}
						this.log.debug(req.resourceType());
						return req.continue();
					});

					page.on('error', err => {
						this.log.error(err, err.stack);
						browser.close();
					});

					_page = page;
				});
			});
		});
	};

	this.stop = () => {
		return _browser.close();
	};

	this.getHomePage = () => {
		return _page.goto(config.baseURL, { waitUntil: 'domcontentloaded' }).then(() => {
			return _page
				.evaluate(() => {
					const articles = [];
					const subStr = 'https://www.economist.com';

					const getArticleData = item => {
						const title = item.querySelector('h3') && item.querySelector('h3').innerText;
						const text = item.querySelector('p') && item.querySelector('p').innerText;
						const url = item.querySelector('h3 > a') && item.querySelector('h3 > a').getAttribute('href');
						const image = item.querySelector('img') && item.querySelector('img').getAttribute('src');

						if (title && url) {
							articles.push({
								title,
								text,
								url: url.includes(subStr) ? url.substring(subStr.length) : url,
								image,
							});
						}
					};

					document.querySelectorAll('.teaser').forEach(getArticleData);
					document.querySelectorAll('.collection-item').forEach(getArticleData);
					document.querySelectorAll('.layout-section-teasers li').forEach(getArticleData);

					return articles;
				})
				.then(articles => {
					return lodash.uniqBy(articles, 'title');
				});
		});
	};

	this.getArticle = url => {
		return _page.goto(config.baseURL + url, { waitUntil: 'domcontentloaded' }).then(() => {
			return _page.evaluate(() => {
				const article = {};
				const texts = [];

				article.title = document.querySelector('.article__headline') && document.querySelector('.article__headline').innerText;
				article.date = document.querySelector('.article__headline') && document.querySelector('.article__headline').innerText;
				article.description = document.querySelector('.article__description') && document.querySelector('.article__description').innerText;
				article.image = document.querySelector('div[itemprop="image"] img') && document.querySelector('div[itemprop="image"] img').getAttribute('src');

				document.querySelectorAll('.article__body-text').forEach(text => {
					if (text.innerText) {
						texts.push(text.innerText);
					}
				});
				article.texts = texts;

				return article;
			});
		});
	};
}
Service.superOf(ScraperService);

module.exports = {
	ScraperService,
};
