const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');

const { Service } = require('../lib/base');
const { getDependencies, promiseGuard, randomString } = require('../lib/utils');

const { Session, Principal } = require('../types/sessions');
const { EVENTS, VERIFICATION_CODE_POOL } = require('../types/consts');
const { CustomError } = require('../types/errors');

const {
	RegisterUserPayload,
	LoginUserPayload,
	UserVerificationEventData,
	UserVerificationPayload,
} = require('../types/auth');

class AuthServiceConfig {
	constructor(/** AuthServiceConfig */ source) {
		this.secret = source.secret;

		// Email verification code length
		this.verification_code_length = 6;
	}
}

/**
 * User authentication service. Handles login and register.
 * @param {AuthServiceConfig} config
 * @param deps
 */
function AuthService(config, deps) {
	config = new AuthServiceConfig(config);
	deps = getDependencies(deps, ['usersService']);

	this._hashPassword = password => {
		return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
	};

	this._generateVerificationCode = () => {
		return randomString(config.verification_code_length, VERIFICATION_CODE_POOL);
	};

	/**
	 * Register new user
	 * @param {RegisterUserPayload} payload
	 */
	this.register = payload => {
		payload = RegisterUserPayload.from(payload);

		payload.password = this._hashPassword(payload.password);
		payload.verification_code = this._generateVerificationCode();

		return deps.usersService.createUser(payload).then(user => {
			this.emit(EVENTS.user_verfication, UserVerificationEventData.from({ user }));
		});
	};

	/**
	 * Login user
	 * @param {LoginUserPayload} payload
	 * @returns {Promise<Principal>}
	 */
	this.login = payload => {
		payload = LoginUserPayload.from(payload);

		return deps.usersService.getUserByEmail(payload.email).then(user => {
			if (!user) {
				throw new UserNotFoundError(payload.email);
			}

			if (!user.verified_at) {
				throw new NotVerifiedError(user.email);
			}

			if (!bcrypt.compareSync(payload.password, user.password)) {
				throw new WrongPasswordError(payload.password);
			}

			const session = Session.from({ access_token: jwt.sign({ ...user }, config.secret) });
			return Principal.from({ user, session });
		});
	};

	/**
	 * Verifies user's email address
	 * @param {UserVerificationPayload} payload
	 * @returns {Promise<Principal>}
	 */
	this.verifyUserEmail = payload => {
		payload = UserVerificationPayload.from(payload);

		return deps.usersService.getUserByEmail(payload.email).then(user => {
			if (payload.verification_code !== user.verification_code) {
				throw new InvalidVerificationCodeError(payload.email, payload.verification_code);
			}

			return deps.usersService.setVerifiedAt(user.email).then(user => {
				const session = Session.from({ access_token: jwt.sign({ ...user }, config.secret) });

				return Principal.from({ user, session });
			});
		});
	};

	/**
	 * Verifies access token
	 * @param {string} accessToken
	 */
	this.verifyAccessToken = accessToken => {
		return promiseGuard(() => {
			return jwt.verify(accessToken, config.secret);
		})
			.then(token => {
				return deps.usersService.getUserByEmail(token.email);
			})
			.catch(err => {
				throw new InvalidAccessTokenError(accessToken);
			});
	};
}

Service.superOf(AuthService);

// ****************************************************************

class UserNotFoundError extends CustomError {
	constructor(email) {
		super(`User ${email} doesn't exist'`);
		this.status = 404;
		this.data = { email };
	}
}

class InvalidAccessTokenError extends CustomError {
	constructor(token) {
		super('Access token is not valid');
		this.status = 401;
		this.data = { token };
	}
}

class NotVerifiedError extends CustomError {
	constructor(email) {
		super(`User ${email} has not been verified`);
		this.status = 401;
		this.data = { email };
	}
}

class WrongPasswordError extends CustomError {
	constructor(password) {
		super(`Wrong password`);
		this.status = 400;
		this.data = { password };
	}
}

class InvalidVerificationCodeError extends CustomError {
	constructor(email, code) {
		super(`Verification code not valid`);
		this.status = 400;
		this.data = { email, code };
	}
}

module.exports = {
	AuthService,
};
