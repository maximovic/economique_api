const { getDependencies } = require('../lib/utils');
const { CustomError } = require('../types/errors');
const { User } = require('../types/users');

const { Service } = require('../lib/base');

function UsersService(config, deps) {
	deps = getDependencies(deps, ['db']);

	/**
	 * Insert user into database
	 * @param {User} user
	 * @returns {Promise}
	 */
	this._insertUser = user => {
		return deps.db.one(
			deps.db.sql`
			INSERT INTO users(username, email, password, verification_code, verified_at, created_at)
			VALUES(${user.username}, ${user.email}, ${user.password}, ${user.verification_code}, NULL, NOW())
			RETURNING *`
		);
	};

	/**
	 * Get user record
	 * @param {string} email
	 * @returns {Promise<User>}
	 */
	this.getUserByEmail = email => {
		return deps.db.maybeOne(
			deps.db.sql`
				SELECT * 
				FROM users 
				WHERE email = ${email}`
		);
	};

	/**
	 * Create new user record
	 * @param {RegisterUserPayload} payload
	 * @return {Promise<User>}
	 */
	this.createUser = payload => {
		return this.getUserByEmail(payload.email).then(user => {
			if (user) {
				throw new UserAlreadyExistsError(user);
			}

			return this._insertUser(payload).then(user => {
				return User.from(user);
			});
		});
	};

	/**
	 * Verify user record
	 * @param email
	 * @returns {Promise<User>}
	 */
	this.setVerifiedAt = email => {
		return deps.db
			.one(
				deps.db.sql`
				UPDATE users 
				SET verified_at = NOW() 
				WHERE email = ${email} 
				RETURNING *`
			)
			.then(user => {
				return User.from(user);
			});
	};
}
Service.superOf(UsersService);

// ****************************************************************

class UserAlreadyExistsError extends CustomError {
	constructor(user) {
		super(`User with '${user.email}' email already exist'`);
		this.status = 400;
		this.data = user;
	}
}

module.exports = {
	UsersService,
};
