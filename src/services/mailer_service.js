const nodemailer = require('nodemailer');

const { Service } = require('../lib/base');
const { CustomError } = require('../types/errors');

class MailerServiceConfig {
	constructor(/** MailerServiceConfig */ source) {
		this.enabled = source.enabled;
		this.service = source.service;
		this.user = source.user;
		this.pass = source.pass;
		this.sender = source.sender;
	}
}

function MailerService(config, deps) {
	config = new MailerServiceConfig(config);

	this.start = () => {
		if (!config.enabled) {
			return;
		}

		this.transport = nodemailer.createTransport({
			service: config.service,
			auth: {
				user: config.user,
				pass: config.pass,
			},
		});

		return this.transport
			.verify()
			.then(() => {
				this.log.info(`mailing set up with ${config.service}`);
			})
			.catch(err => {
				this.log.error(new EmailTransportNotValidError(config, err));
			});
	};

	this.sendEmail = (receiver, mail) => {
		return this.transport
			.sendMail({
				from: `"${config.sender}" <${config.user}>`,
				to: receiver,
				subject: mail.subject,
				text: mail.body,
			})
			.catch(err => {
				throw new SendEmailFailedError(receiver, mail, err);
			});
	};
}
Service.superOf(MailerService);

class EmailTransportNotValidError extends CustomError {
	constructor(config, err) {
		super(`Failed to create ${config.service} email transport for user ${config.user}`);
		this.status = 500;
		this.data = { config };
		this.innerError = err;
	}
}

class SendEmailFailedError extends CustomError {
	constructor(receiver, mail, err) {
		super(`Failed to send email to ${receiver}`);
		this.status = 400;
		this.data = { receiver, mail };
		this.innerError = err;
	}
}

module.exports = {
	MailerService,
};
