exports.ENV = {
	dev: 'dev',
	prod: 'prod',
	migrate: 'migrate',
};

exports.SIGNALS = {
	SIGINT: 'SIGINT',
	SIGTERM: 'SIGTERM',
};

exports.HTTP_STATUS_CODE = {
	NOT_FOUND: '404',
	INTERNAL: '500',
	BAD_REQUEST: '400',
};
exports.HTTP_ERROR_MESSAGE = {
	[exports.HTTP_STATUS_CODE.NOT_FOUND]: 'Not Found',
	[exports.HTTP_STATUS_CODE.INTERNAL]: 'Internal Server Error',
};

exports.SKEMA_ERROR_CODES = {
	UNKNOWN_TYPE: 'UNKNOWN_TYPE',
};
exports.SKEMA_ERROR_CODES_LIST = Object.keys(exports.SKEMA_ERROR_CODES);

exports.EVENTS = {
	user_verfication: 'user_verfication',
};

exports.VERIFICATION_CODE_POOL = '123456789ABCDEFGHJKLMNPQRSTUWXYZ';
