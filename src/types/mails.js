class VerificationMail {
	constructor(/** VerificationMail */ source) {
		this.subject = source.subject || 'Account verification';
		this.body = `Hello ${source.username}, \nWelcome to Economique. To verify your account enter the code below on the website. \n${source.code}`;
	}
}

module.exports = {
	VerificationMail,
};
