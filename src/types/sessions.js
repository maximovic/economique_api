const { Schema } = require('./skemas');
const { User } = require('./users');

const Session = Schema({
	access_token: 'string',
});

const Principal = Schema({
	user: User,
	session: Session,
});

module.exports = {
	Session,
	Principal,
};
