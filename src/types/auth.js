const { User } = require('./users');
const { Schema, Email } = require('./skemas');

const RegisterUserPayload = Schema({
	username: 'string',
	email: Email,
	password: 'string',
});

const LoginUserPayload = Schema({
	email: Email,
	password: String,
});

const UserVerificationPayload = Schema({
	email: Email,
	verification_code: 'string',
});

const UserVerificationEventData = Schema({
	user: User,
});

module.exports = {
	RegisterUserPayload,
	LoginUserPayload,
	UserVerificationPayload,
	UserVerificationEventData,
};
