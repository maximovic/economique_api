const { Schema, Email } = require('./skemas');

const User = Schema({
	id: 'number?',
	username: 'string',
	email: Email,
	password: 'string?',
	verification_code: 'string?',
	verified_at: 'number?',
	created_at: 'number',
});

module.exports = {
	User,
};
