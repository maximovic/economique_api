const lodash = require('lodash');

const { defaults, type } = require('skema');
const { LOOSE } = require('@skema/basic');

const { SkemaInitializationError, InvalidDateError, InvalidEmailError } = require('./errors');

const EMAIL_REGEX = new RegExp(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);

const { shape } = defaults({
	// async: true,
	types: LOOSE,
});

const Schema = object => {
	try {
		return shape(object, true);
	} catch (err) {
		throw new SkemaInitializationError(err);
	}
};

const Date = type({
	type: 'string',
	validate: date => {
		if (!date) {
			return null;
		}

		if (date && !lodash.isDate(date)) {
			throw new InvalidDateError(date);
		}
	},
});

const Email = type({
	type: 'string',
	validate: email => {
		if (!email.match(EMAIL_REGEX)) {
			throw new InvalidEmailError(email);
		}
	},
});

module.exports = {
	Schema,

	Date,
	Email,
};
