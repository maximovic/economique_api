const { HTTP_STATUS_CODE, ROLES_LIST } = require('./consts');

class CustomError extends Error {
	constructor(message, status, innerError) {
		super(message);
		Object.setPrototypeOf(this, new.target.prototype);

		/**
		 * Ensure the name of this error is the same as the class name
		 * @type {string}
		 */
		this.name = this.constructor.name;

		/**
		 * Error datetime
		 * @type {Date}
		 */
		this.timestamp = new Date();

		/**
		 * Error status code
		 * @type {number}
		 */
		this.status = status ? status : HTTP_STATUS_CODE.INTERNAL;

		/**
		 * Error that started the chain
		 * @type {Error}
		 */
		this.innerError = innerError ? innerError : null;

		/**
		 * Error stack trace
		 */
		this.stack = innerError ? innerError.stack : null;

		// This clips the constructor invocation from the stack trace.
		// It's not absolutely essential, but it does make the stack trace a little nicer.
		Error.captureStackTrace(this, this.constructor);
	}
}

class SkemaError extends CustomError {
	constructor(message, status, skemaError) {
		super(message, status, skemaError);

		/**
		 * Skema error arguments
		 * @type {Array}
		 */
		this.args = skemaError.args || [];
	}
}

class SkemaInitializationError extends SkemaError {
	constructor(err) {
		super(`Failed to initialize skema. ${err.message}`, HTTP_STATUS_CODE.INTERNAL, err);
	}
}

class SkemaValidationError extends SkemaError {
	constructor(err) {
		super(err.message, HTTP_STATUS_CODE.BAD_REQUEST, err);
	}
}

class InvalidDateError extends CustomError {
	constructor(date) {
		super(`'${date}' is not a valid date.`);
		this.status = 400;
		this.data = { date };
	}
}

class InvalidEmailError extends CustomError {
	constructor(email) {
		super(`'${email}' is not a valid email address.`);
		this.status = 400;
		this.data = { email };
	}
}

class InvalidRoleError extends CustomError {
	constructor(role) {
		super(`'${role}' is not a valid user role. It must be one of the following: ${ROLES_LIST.join(', ')}`);
		this.status = 400;
		this.data = { role };
	}
}

module.exports = {
	CustomError,

	SkemaError,
	SkemaInitializationError,
	SkemaValidationError,

	InvalidDateError,
	InvalidEmailError,
	InvalidRoleError,
};
