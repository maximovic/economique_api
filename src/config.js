const fs = require('fs');
const yaml = require('js-yaml');

const loadConfigSync = env => {
	try {
		const file = fs.readFileSync(`./config/${env}.yaml`, 'utf8');
		return yaml.safeLoad(file);
	} catch (err) {
		console.log(err);
	}
};

class AppConfig {
	constructor(/** AppConfig */ source) {
		/** @type {string} */
		this.env = source.env;
	}
}

module.exports = {
	loadConfigSync,

	AppConfig,
};
