const { Service } = require('../../lib/base');
const { getDependencies } = require('../../lib/utils');

function AuthMiddleware(config, deps) {
	deps = getDependencies(deps, ['server', 'authService']);

	this.use = (req, res, next) => {
		return Promise.resolve()
			.then(() => {
				if (!req.headers.authorization) {
					throw new MissingAuthHeaderError('Missing auth headers');
				}
			})
			.then(() => {
				const token = req.headers.authorization.split(' ')[1];
				return deps.authService.verifyAccessToken(token);
			})
			.then(user => {
				res.locals.user = user;
				return next();
			})
			.catch(err => {
				return res.status(err.status).send(err.message);
			});
	};
}
Service.superOf(AuthMiddleware);

class MissingAuthHeaderError extends Error {
	constructor(/** MissingAuthHeaderError */ message) {
		super();
		this.status = 400;
		this.message = message;
	}
}

module.exports = {
	AuthMiddleware,
};
