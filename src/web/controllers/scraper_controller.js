function ScraperController(deps) {
	return deps.server
		.createRouter('/scrape')

		.get('/homepage', deps.authMiddleware.use, (req, res, next) => {
			return deps.scraperService
				.getHomePage()
				.then(result => {
					return res.send(result);
				})
				.catch(err => {
					next(err);
				});
		})

		.get('/article/:url*', deps.authMiddleware.use, (req, res, next) => {
			return deps.scraperService
				.getArticle(req.params.url + req.params[0])
				.then(result => {
					return res.send(result);
				})
				.catch(err => {
					next(err);
				});
		});
}

module.exports = {
	ScraperController,
};
