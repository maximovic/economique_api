function AuthController(deps) {
	return deps.server
		.createRouter('/auth')

		.post('/login', (req, res, next) => {
			return deps.authService
				.login(req.body)
				.then(user => {
					return res.send(user);
				})
				.catch(err => {
					next(err);
				});
		})

		.post('/register', (req, res, next) => {
			return deps.authService
				.register(req.body)
				.then(user => {
					return res.send(user);
				})
				.catch(err => {
					next(err);
				});
		})

		.put('/verification', (req, res, next) => {
			return deps.authService
				.verifyUserEmail(req.body)
				.then(user => {
					res.send(user);
				})
				.catch(err => {
					next(err);
				});
		});
}

module.exports = {
	AuthController,
};
