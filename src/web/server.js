const express = require('express');
const logger = require('morgan');
const cors = require('cors');
const { createHttpTerminator } = require('http-terminator');

const { Service } = require('../lib/base');
const { HTTP_STATUS_CODE } = require('../types/consts');
const { CustomError, SkemaValidationError } = require('../types/errors');

class ServerConfig {
	constructor(/** ServerConfig */ source) {
		this.enabled = source.enabled;
		this.port = process.env.PORT || source.port;
		this.swagger = source.swagger;
	}
}

function Server(config) {
	config = new ServerConfig(config);

	// init app
	const app = express();

	// swagger ui config
	this.swagger = config.swagger;

	app.use(cors());
	app.use(logger('dev'));
	app.use(express.json());
	app.use(express.urlencoded({ extended: true }));

	this._handle404 = (req, res) => {
		return res.sendStatus(HTTP_STATUS_CODE.NOT_FOUND);
	};

	this._handleError = (error, req, res, next) => {
		this.log.error(error.stack);

		if (error instanceof CustomError) {
			return res.status(error.status).json({
				message: error.message,
				status: error.status,
				timestamp: error.timestamp,
				data: error.data,
			});
		}

		if (error[Symbol.for('skema:error')]) {
			error = new SkemaValidationError(error);

			return res.status(error.status).json({
				message: error.message,
				status: error.status,
				timestamp: error.timestamp,
				data: error.data,
			});
		}

		return res.sendStatus(HTTP_STATUS_CODE.INTERNAL);
	};

	this.start = () => {
		if (!config.enabled) {
			return;
		}

		app.use(this._handle404);
		app.use(this._handleError);

		this.terminator = createHttpTerminator({
			server: app.listen(config.port, () => this.log.info(`server running on port ${config.port}`)),
		});
	};

	this.stop = () => {
		if (!config.enabled) {
			return;
		}

		return this.terminator
			.terminate()
			.then(() => {
				this.log.info('server stopped running');
				process.exit(0);
			})
			.catch(error => {
				this.log.error(error);
				process.exit(1);
			});
	};

	this.createRouter = url => {
		if (!url.match(/\/./)) {
			this.log.warn(`route '${url}' must start with a '/'`);
		}

		const router = new express.Router();
		app.use(url, router);

		return router;
	};
}

Service.superOf(Server);

module.exports = {
	Server,
};
