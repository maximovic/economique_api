const { SIGNALS } = require('./types/consts');

const loadDependencies = () => {
	const dependencies = {
		Server: require('./web/server').Server,
		Database: require('./lib/database').Database,

		UsersService: require('./services/users_service').UsersService,
		AuthService: require('./services/auth_service').AuthService,

		MailerService: require('./services/mailer_service').MailerService,
		MailAdapter: require('./services/mail_adapter').MailAdapter,

		ScraperService: require('./services/scraper_service').ScraperService,

		AuthMiddleware: require('./web/middleware/auth_middleware').AuthMiddleware,

		AuthController: require('./web/controllers/auth_controller').AuthController,
		ScraperController: require('./web/controllers/scraper_controller').ScraperController,
	};

	return dependencies;
};

/**
 * Main application class. Contains all configurations and logic.
 * @param {AppConfig} config
 * @param {Object} deps
 */
class App {
	constructor(config, deps = undefined) {
		deps = deps || loadDependencies();

		this.config = config;
		this.services = [];

		this.server = this._createService(deps.Server);
		this.db = this._createService(deps.Database);

		this.usersService = this._createService(deps.UsersService);
		this.authService = this._createService(deps.AuthService);

		this.mailerService = this._createService(deps.MailerService);
		this.mailAdapter = this._createService(deps.MailAdapter);

		this.scraperService = this._createService(deps.ScraperService);

		this.authMiddleware = this._createService(deps.AuthMiddleware);

		this.authController = deps.AuthController(this);
		this.scraperController = deps.ScraperController(this);

		this._startServices();

		process.on(SIGNALS.SIGINT, () => this._stopServices());
		process.on(SIGNALS.SIGTERM, () => this._stopServices());

		process.on('uncaughtException', function (err) {
			console.log('Uncaught Exception:', err);
			process.exit(1); // This is VITAL. Don't swallow the err and try to continue.
		});
	}

	_createService(Ctr) {
		const config = this.config[Ctr.name];
		const service = new Ctr(config, this);
		this.services.push(service);

		return service;
	}

	_startServices() {
		this.services.forEach(service => {
			try {
				service.start();
			} catch (err) {
				console.error(err);
			}
		});
	}

	_stopServices() {
		this.services.forEach(service => {
			service.stop();
		});
	}
}

module.exports = {
	App,
};
